import { App, AsyncComponentLoader } from 'vue'
import uniqid from 'uniqid'
import { create } from 'amo-client-axios'
import { AxiosInstance } from 'axios'

export class Widget {
  constructor(private amoWidget: any) {
    this.api = create(this.clientId, {
      baseURL: process.env.API_URL
    })
  }

  private modalApp: App
  private uniqId = uniqid(`modal-`)
  private api: AxiosInstance

  onRender() {
    if (!this.isCard || !this.isLeads) return
    console.log('я в карточке сделки')

    const menu = document.querySelector('.card-fields__top-name-more .button-input__context-menu ')
    const li = document.createElement('li')
    li.innerHTML = `
      <div class="button-input__context-menu__item__inner">
        <span class="button-input__context-menu__item__text ">Копировать</span>
      </div>
    `
    li.classList.add('button-input__context-menu__item')
    li.addEventListener('click', () => this.openModal([AMOCRM.data.current_card.id]))

    menu.prepend(li)
  }

  get clientId() {
    return this.amoWidget.params.oauth_client_uuid
  }

  get isCard(): boolean {
    return AMOCRM.isCard() as boolean
  }

  get isLeads(): boolean {
    return AMOCRM.getBaseEntity() === 'leads'
  }

  async openModal(leadIds: number[]) {
    if (this.modalApp) this.modalApp.unmount()

    if (!this.isCard) document.querySelectorAll('#widgets_block, #card_widgets_overlay').forEach(el => el.remove())

    let div = document.querySelector(`#${this.uniqId}`)
    if (!div) {
      div = document.createElement('div')
      div.id = this.uniqId
      document.querySelector(this.isCard ? '#card_holder' : '#list_page_holder').appendChild(div)
    }

    this.modalApp = await this.createApp(() => import('./components/Modal.vue'), { leadIds, api: this.api })
    this.modalApp.mount(div)
  }

  private async createApp(component: AsyncComponentLoader, props?: any) {
    const { createApp, defineAsyncComponent } = await import('vue')
    return createApp(defineAsyncComponent(component), props)
  }
}
