import { Widget } from './widget'

export default function (amoWidget: any) {
  let widget: Widget
  const getWidget = async () => {
    if (process.env.NODE_ENV === 'production') {
      __webpack_public_path__ = `${amoWidget.params.path}/build/`
    }
    if (!widget) {
      widget = new Widget(amoWidget)
    }

    return widget
  }

  amoWidget.callbacks = {
    settings($box) {
      if (amoWidget.params.status === 'not_configured') {
        amoWidget.background_install()
      }
    },
    init() {
      return true
    },
    bind_actions() {
      return true
    },
    async render() {
      ;(await getWidget()).onRender()
      return true
    },
    dpSettings() {
      return true
    },
    advancedSettings() {
      return true
    },
    destroy() {},
    contacts: {
      selected() {
        return true
      }
    },
    leads: {
      async selected() {
        const { selected } = amoWidget.list_selected()
        const leadIds: number[] = selected.map(({ id }) => id)
        await (await getWidget()).openModal(leadIds)
        return true
      }
    },
    todo: {
      selected() {
        return true
      }
    },
    onSave() {}
  }

  return amoWidget
}
