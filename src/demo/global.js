AMOCRM.constant('account')
AMOCRM.constant('user')
AMOCRM.constant('managers')

AMOCRM.isCard()
AMOCRM.data.current_list
AMOCRM.data.current_list.models.filter(m => m.attributes.budget > 0)

// Удалить
AMOCRM.data.current_card.tabs._tabs.splice(1, 1)
$(document).trigger('resize')

// Добавить
AMOCRM.data.current_card.tabs._tabs.push({
  id: 'test',
  name: 'Hello World!',
  show: true
})
$(document).trigger('resize')

AMOCRM.data.current_card.tabs.switchTab('test')

AMOCRM.data.card_page.notes._compose.setCompose('task')
AMOCRM.data.card_page.notes._compose.setCompose('note')
AMOCRM.data.card_page.notes._compose.setCompose('email')

AMOCRM.data.card_page.notes._compose.edit()

AMOCRM.router.navigate('/dashboard/', { trigger: true })
AMOCRM.router.navigate('/leads/detail/33903741/', { trigger: true })
