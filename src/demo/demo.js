import { createApp } from 'vue'
import Demo from './Demo.vue'

export function addOpacityToCustomFields() {
  $('.linked-form__field').css({ opacity: '0.5' })
  //   $('.linked-form__field').hide()
}

export function addCardObserver() {
  const observer = new MutationObserver((mutationsList, observer) => {
    console.log('Работает Mutation Observer карточки')
    mutationsList.forEach(mutation => {
      mutation.addedNodes.forEach(node => {
        if (node.id === 'card_fields') {
          addOpacityToCustomFields()
        }
      })
    })
  })
  const target = document.querySelector('#card_holder')
  const config = { childList: true }
  observer.observe(target, config)
}

let cardObserver
export function addCardObserverRouter() {
  if (cardObserver) cardObserver.disconnect()

  const target = document.querySelector('#card_holder')
  const findId = 'card_fields'

  if (target && target.querySelector(`#${findId}`)) {
    return addOpacityToCustomFields()
  }

  cardObserver = new MutationObserver((mutationsList, observer) => {
    console.log('Работает Mutation Observer карточки (роутер)')
    mutationsList.forEach(mutation => {
      mutation.addedNodes.forEach(node => {
        if (node.id === findId) {
          addOpacityToCustomFields()
          observer.disconnect()
        }
      })
    })
  })

  const config = { childList: true }
  cardObserver.observe(target, config)
}

export function addPipelineObserver() {
  const target = document.querySelector('body')
  const observer = new MutationObserver((mutationsList, observer) => {
    console.log('Работает Mutation Observer воронки')
    mutationsList.forEach(mutation => {
      mutation.addedNodes.forEach(node => {
        if (node.id === 'page_holder') {
          $('.pipeline_leads__item').css({ opacity: '0.5' })
        }
      })
    })
  })
  const config = { childList: true, subtree: true }
  observer.observe(target, config)
}

let pipelineObserver
export function addPipelineObserverRouter() {
  if (pipelineObserver) pipelineObserver.disconnect()

  const target = document.querySelector('body')
  const findClass = 'pipeline_leads__item'
  const action = () => $(`.${findClass}`).css({ opacity: '0.5' })

  if (target && target.querySelector(`.${findClass}`)) {
    return action()
  }

  pipelineObserver = new MutationObserver((mutationsList, observer) => {
    console.log('Работает Mutation Observer воронки (роутер)')
    mutationsList.forEach(mutation => {
      mutation.addedNodes.forEach(node => {
        if (node.id === 'page_holder') {
          action()
          observer.disconnect()
        }
      })
    })
  })

  const config = { childList: true, subtree: true }
  pipelineObserver.observe(target, config)
}

export function patchRouter() {
  const original = AMOCRM.router.navigate

  AMOCRM.router.navigate = (...args) => {
    const [pathname = ''] = args

    if (pathname.match(/^\/leads\/detail\/\d+/)) {
      console.log('Переходим в сделку')
      addCardObserverRouter()
    } else if (pathname.match(/^\/leads\/pipeline\/\d+/)) {
      console.log('Переходим в воронку')
      addPipelineObserverRouter()
    }

    return original.apply(this, args)
  }
  console.log('router patched')
}

export function createVueApp() {
  const app = createApp(Demo)
  app.mount('#page_holder')
}

export async function createVueAppAsync() {
  const { createApp, defineAsyncComponent } = await import('vue')
  const app = createApp(defineAsyncComponent(() => import('./Demo.vue')))
  app.mount('#page_holder')
}
