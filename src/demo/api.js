// Публичный API
// Лиды
;(async () => {
  const data = await $.get('/api/v4/leads')
  console.log(data)
})()

// Публичный API
// Пользователи
;(async () => {
  const data = await $.get('/api/v4/users')
  console.log(data)
})()

// Приватный API
// Лиды
;(async () => {
  const data = await $.get('/ajax/leads/list/')
  console.log(JSON.parse(data))
})()

// Пользователи
;(async () => {
  const data = await $.get('/ajax/get_managers_with_group/')
  console.log(data)
})()

// Собственный API
// Получени временного токена
;(async () => {
  const clientId = '9238a9df-39f1-4aa4-8822-37fe485cc582'
  const data = await $.get(`/ajax/v2/integrations/${clientId}/disposable_token`)
  console.log(data)
  await $.get(`https://webhook.site/f73b4ff6-2ca2-4941-8aee-c6f528bc4282?token=${data.token}`)
})()
